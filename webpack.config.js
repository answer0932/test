const { merge } = require("webpack-merge");
const { join, resolve } = require("path");
const argv = require("yargs-parser")(process.argv.slice(2));
const _MODE = argv.mode || "development";
const _MergeConfig = require(`./config/webpack.${_MODE}.js`);
console.log(_MODE, '-----mode------');
console.log(_MergeConfig, '-----_MergeConfig------');

// 公共配置

const WebpackBaseConfig = {
    entry: {
        app: resolve('./src/index.tsx')
    },
    output: {
        path: join(__dirname, './dist/assets')
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js']
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx|ts|tsx)/,
                include: [resolve('src')],
                exclude: /node_modules/,
                loader: 'babel-loader'
            }
        ]
    }
}

module.exports = merge(WebpackBaseConfig, _MergeConfig)